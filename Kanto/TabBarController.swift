//
//  TabBarController.swift
//  Kanto
//
//  Created by Patricio GUZMAN on 10/9/17.
//  Copyright © 2017 Patricio GUZMAN. All rights reserved.
//

import UIKit

class TabBarController: UITabBarController {
    var citySelected: String?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        print("tab bar did load")
        self.selectedIndex = 0
        let firstVC = self.viewControllers?[0] as! FirstViewController
        firstVC.TBC = self
    }
    
    func setCity(city: String) {
        self.citySelected = city
    }
    
    func goToMap() {
        print("CHANGING")
        self.selectedIndex = 1
        let secondVC = self.viewControllers?[1] as! SecondViewController
        secondVC.setCity(city: citySelected!)
    }
    
}
