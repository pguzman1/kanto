//
//  FirstViewController.swift
//  Kanto
//
//  Created by Patricio GUZMAN on 10/9/17.
//  Copyright © 2017 Patricio GUZMAN. All rights reserved.
//

import UIKit

class FirstViewController: UIViewController {

    @IBOutlet weak var table: UITableView!
    var fakePlaces: [String] = ["Paris", "Santiago", "Puerto Natales"]
    var TBC: TabBarController!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        table.delegate = self
        print("did load 1")
    }

}

extension FirstViewController: UITableViewDelegate, UITableViewDataSource {
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = table.dequeueReusableCell(withIdentifier: "prototype1") as! Prototype1TableViewCell
        cell.setCell(label: fakePlaces[indexPath.row])
        return cell
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return self.fakePlaces.count
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        print(fakePlaces[indexPath.row])
        TBC.setCity(city: fakePlaces[indexPath.row])
        TBC.goToMap()
    }
}
