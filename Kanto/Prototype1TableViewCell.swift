//
//  Prototype1TableViewCell.swift
//  Kanto
//
//  Created by Patricio GUZMAN on 10/9/17.
//  Copyright © 2017 Patricio GUZMAN. All rights reserved.
//

import UIKit

class Prototype1TableViewCell: UITableViewCell {

    @IBOutlet weak var myLabel: UILabel!
    
    func setCell(label: String) {
        self.myLabel.text = label
    }
}

//protocol Prototype1TableViewCellDelegate {
//    func setCell()
//}
