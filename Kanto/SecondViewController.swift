//
//  SecondViewController.swift
//  Kanto
//
//  Created by Patricio GUZMAN on 10/9/17.
//  Copyright © 2017 Patricio GUZMAN. All rights reserved.
//

import UIKit
import MapKit

class SecondViewController: UIViewController {
    
    @IBOutlet weak var segmentedControl: SegmentedControl!
    @IBOutlet weak var map: MKMapView!
    var mapController: MapController?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        print("did load 2")

        mapController = MapController(map: map)
        mapController!.setup()
        mapController!.setupDefaultRegion()
    }
    
    @IBAction func indexChanged(_ sender: SegmentedControl) {
        switch sender.selectedSegmentIndex {
        case 0:
            mapController?.changeMode(to: mapType.Hybrid)
        case 1:
            mapController?.changeMode(to: mapType.Satellite)
        case 2:
            mapController?.changeMode(to: mapType.Standard)
        default:
            print("s")
        }
    }
    
    @IBAction func clickGeo(_ sender: UIButton) {
        mapController?.goToUser()
    }
    
    func setCity(city: String) {
        switch city {
        case "Santiago":
            let Santiago2D = CLLocationCoordinate2DMake(santiago[0], santiago[1])
            mapController?.setMyRegion(spanLatLCDegrees: 0.1, spanLonLCDegrees: 0.1, location: Santiago2D)
        case "Puerto Natales":
            let PuertoNatales2D = CLLocationCoordinate2DMake(puerto_natales[0], puerto_natales[1])
            mapController?.setMyRegion(spanLatLCDegrees: 0.1, spanLonLCDegrees: 0.1, location: PuertoNatales2D)
        case "Paris":
            let Paris2D = CLLocationCoordinate2DMake(paris[0], paris[1])
            mapController?.setMyRegion(spanLatLCDegrees: 0.1, spanLonLCDegrees: 0.1, location: Paris2D)
        default:
            print("THERE IS NO SUCH CITY")
        }
        
    }
}
