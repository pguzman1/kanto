//
//  File.swift
//  Kanto
//
//  Created by Patricio GUZMAN on 10/9/17.
//  Copyright © 2017 Patricio GUZMAN. All rights reserved.
//

import Foundation
import MapKit
import CoreLocation

enum mapType: Int {
    case Hybrid = 1, Satellite, Standard
}

let ecole42: [CLLocationDegrees] = [48.8966, 2.3185]
let santiago: [CLLocationDegrees] = [-33.4489, -70.6693]
let paris: [CLLocationDegrees] = [48.8584, 2.2945]
let puerto_natales: [CLLocationDegrees] = [-51.7309, -72.4977]

class MapController: NSObject {
    var map: MKMapView!
    var manager: CLLocationManager!
    var userLocation: CLLocation?
    
    init(map: MKMapView) {
        self.map = map
        self.manager = CLLocationManager()
    }
    
    func setup() {
        map.delegate = self
        manager.delegate = self
        manager.desiredAccuracy = kCLLocationAccuracyBest
        manager.requestAlwaysAuthorization()
        manager.startUpdatingLocation()
    }
    
    func changeMode(to: mapType) {
        switch to {
        case .Hybrid:
            map.mapType = MKMapType.hybrid
        case .Satellite:
            map.mapType = MKMapType.satellite
        case .Standard:
            map.mapType = MKMapType.standard
        }
    }
    
    func setMyRegion(spanLatLCDegrees: CLLocationDegrees, spanLonLCDegrees: CLLocationDegrees, location: CLLocationCoordinate2D) {
        let span: MKCoordinateSpan = MKCoordinateSpanMake(spanLatLCDegrees, spanLonLCDegrees)
        let location: CLLocationCoordinate2D = location
        let region:MKCoordinateRegion = MKCoordinateRegionMake(location, span)
        map.setRegion(region, animated: true)
    }
    
    func setupDefaultRegion() {
        let location: CLLocationCoordinate2D = CLLocationCoordinate2DMake(ecole42[0], ecole42[1])
        let locationSantiago: CLLocationCoordinate2D = CLLocationCoordinate2DMake(santiago[0], santiago[1])
        let locationPuerto: CLLocationCoordinate2D = CLLocationCoordinate2DMake(puerto_natales[0], puerto_natales[1])
        let locationParis: CLLocationCoordinate2D = CLLocationCoordinate2DMake(paris[0], paris[1])
        
        //        self.setMyRegion(spanLatLCDegrees: 0.1, spanLonLCDegrees: 0.1, location: location)
        
        var title = "Ecole 42"
        var subtitle = "i'm here"
        self.createAnnotation(title: title, subtitle: subtitle, coordinate: location)
        title = "Santiago"
        subtitle = "where i lived"
        self.createAnnotation(title: title, subtitle: subtitle, coordinate: locationSantiago)
        title = "Puerto Natales"
        subtitle = "Nice place"
        self.createAnnotation(title: title, subtitle: subtitle, coordinate: locationPuerto)
        title = "Paris"
        subtitle = "where i live"
        self.createAnnotation(title: title, subtitle: subtitle, coordinate: locationParis)
    }
    
    func createAnnotation(title: String, subtitle: String, coordinate: CLLocationCoordinate2D) {
        let annotation = MKPointAnnotation()
        annotation.coordinate = coordinate
        annotation.title = title
        annotation.subtitle = subtitle
        map.addAnnotation(annotation)
    }
    
    func goToUser() {
        if userLocation != nil {
            let myLocation: CLLocationCoordinate2D = CLLocationCoordinate2DMake(userLocation!.coordinate.latitude, userLocation!.coordinate.longitude)
            self.setMyRegion(spanLatLCDegrees: 0.1, spanLonLCDegrees: 0.1, location: myLocation)
            map.showsUserLocation = true
        }
    }
    
}

extension MapController: MKMapViewDelegate {
    
    func mapView(_ mapView: MKMapView, viewFor annotation: MKAnnotation) -> MKAnnotationView? {
        if annotation is MKUserLocation {
            return nil
        }
//        var annotationView = mapView.dequeueReusableAnnotationView(withIdentifier: "indentifier")
        var annotationView = mapView.dequeueReusableAnnotationView(withIdentifier: "pin")
        
        
        if annotationView == nil {
            //                    annotationView = MKAnnotationView(annotation: annotation, reuseIdentifier: "indentifier")
            annotationView = MKPinAnnotationView(annotation: annotation, reuseIdentifier: "pin")
            annotationView?.canShowCallout = true
        } else {
            annotationView?.annotation = annotation
        }
        let title: String? = annotation.title!
        if title == "Santiago" {
            annotationView?.image = #imageLiteral(resourceName: "icons8-Chile-48")
        }
        else if title == "Puerto Natales" {
            let anot = annotationView as! MKPinAnnotationView
            anot.pinTintColor = UIColor.green
//                        annotationView?.image = #imageLiteral(resourceName: "icons8-Chile-48")
        }
        else if title == "Ecole 42" {
            let anot = annotationView as! MKPinAnnotationView
            anot.pinTintColor = UIColor.green
//            annotationView?.image = #imageLiteral(resourceName: "icons8-42 Filled-50")
        }
        else if title == "Paris" {
            annotationView?.image = #imageLiteral(resourceName: "icons8-Eiffel Tower Filled-50")
        }
        annotationView?.annotation = annotation
        annotationView?.canShowCallout = true
        return annotationView
    }
}

extension MapController: CLLocationManagerDelegate {
    
    func locationManager(_ manager: CLLocationManager, didUpdateLocations locations: [CLLocation]) {
        let len = locations.count
        if (len > 0) {
            self.userLocation = locations[len - 1]
        }
        //        let span:MKCoordinateSpan = MKCoordinateSpanMake(0.01, 0.01)
        //        let myLocation: CLLocationCoordinate2D = CLLocationCoordinate2DMake(location.coordinate.latitude, location.coordinate.longitude)
        //        let region: MKCoordinateRegion = MKCoordinateRegionMake(myLocation, span)
        //        map.setRegion(region, animated: true)
    }
    
    func locationManager(_ manager: CLLocationManager, didFailWithError error: Error) {
        print("")
    }
}

protocol MapControllerDelegate {
    
}
